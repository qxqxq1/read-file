# -*- coding：utf8 -*-
# @Project : read-file
# @Author : qxq
# @Time : 2021/12/17 10:41
import os

from flask import Flask, jsonify, render_template
from flask_cors import CORS

from read_file.exceptions import handle_exception
from read_file.middlewares import MiddlewareManger
from read_file.tools.utils import timestamp2time
from log import LoggerManager
from utils.share import SharedMemory


middle_manger = MiddlewareManger()

log_manager = LoggerManager()


def create_app():
    app = Flask(__name__, static_url_path='')
    app.config.from_object(SharedMemory.Config)

    CORS(app)
    middle_manger.init_app(app)

    @app.errorhandler(Exception)
    def handle_exceptions(error):
        return handle_exception(error)

    @app.route('/')
    def index():
        return jsonify({
            "code": 200, "app_name":  SharedMemory.Config.APP_NAME
        })

    @app.route('/test/')
    def test():
        conf_path = SharedMemory.Config.CONF_PATH
        return jsonify({'code': 200, 'mtime': timestamp2time(os.path.getmtime(conf_path)),'msg': SharedMemory.Config.APP_NAME})


    from read_file.controller import user_blue
    app.register_blueprint(user_blue, url_prefix='/api')

    return app


__all__ = ['create_app']
