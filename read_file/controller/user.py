# -*- coding：utf8 -*-
# @Project : read-file
# @Author : qxq
# @Time : 2022/1/14 10:33
from flask import g

from read_file.controller import user_blue
from read_file.tools.resp_code import Response, StatusCode


@user_blue.route('/login/', methods=['GET'])
def login():
    data = {
        "api": "api/login"
    }

    return Response.success(data=data)


@user_blue.route('/user/', methods=['GET'])
def user():
    data = {
        "api": "api/user"
    }

    return Response.success(data=data)
