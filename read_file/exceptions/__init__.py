# -*- coding：utf8 -*-
# @Project : read-file
# @Author : qxq
# @Time : 2021/12/17 10:41
import sys
import traceback

from log import record_system_log
from read_file.tools.resp_code import Response
from werkzeug.exceptions import NotFound, MethodNotAllowed
from flask import jsonify, request


class APIException(Exception):
    def __init__(self, msg=None, error_code=4001, status_code=200, payload=None):
        Exception.__init__(self)
        self.msg = None
        if isinstance(error_code, tuple):
            self.error_code = error_code[0]
            self.msg = error_code[1]
        elif isinstance(error_code, int):
            self.error_code = error_code
        else:
            self.error_code = 4001

        if msg is not None:
            self.msg = msg
        if not self.msg:
            self.msg = 'server missing...'

        self.status_code = status_code
        self.payload = payload

    def __str__(self, *args, **kwargs):  # real signature unknown
        """ Return str(self). """
        return self.msg

    def to_dict(self):
        resp = dict()
        if self.payload is not None:
            resp['data'] = dict(self.payload or ())
        resp['code'] = self.error_code
        resp['msg'] = self.msg
        return resp


def handle_exception(error):
    if isinstance(error, NotFound):
        resp = Response.not_found()
        resp.status_code = 200
    elif isinstance(error, MethodNotAllowed):
        resp = Response.not_allowed()
        resp.status_code = 200

    elif isinstance(error, APIException):
        resp = jsonify(error.to_dict())
        resp.status_code = 200
    else:
        resp = Response.server_error()
        resp.status_code = 200

    # 系统日志记录
    ex_type, ex_val, ex_stack = sys.exc_info()
    filename = ''
    lineno = ''
    for idx, stack in enumerate(traceback.extract_tb(ex_stack)):
        if idx == len(traceback.extract_tb(ex_stack)) - 1:
            filename = stack.filename
            lineno = stack.lineno
    params = {
        'type': 'handle_exceptions',
        'filename': filename,
        'lineno': lineno,
    }
    record_system_log(error, params, request=request)
    return resp
