# -*- coding：utf8 -*-
# @Project : read-file
# @Author : qxq
# @Time : 2021/12/17 10:41
import datetime
import hashlib
import time


def md5(str):
    """
    MD5加密
    :param str:
    :return:
    """
    return hashlib.new('md5', bytes(str, encoding='utf-8')).hexdigest()


def list2map(data, key):
    """
    根据key将map list重新生成新的map
    :param data: [{'id': 1, 'name': 'test'}, {'id': 2, 'name': 'test2'}]
    :param key: map所对应的key，如id，name
    :return: {1: {'id': 1, 'name': 'test'}, 2: {'id': 2, 'name': 'test2'}}
    """
    result = {}

    for item in data:
        if key not in item.keys():
            continue

        result[item[key]] = item

    return result


def list2map_list(data, key):
    """
    根据key将 list map 重新生成新的 map list
    :param data: [{'id': 1, 'name': 'test'}, {'id': 2, 'name': 'test2'}, {'id': 2, 'name': 'test3'}]
    :param key: map所对应的key，如id，name
    :return: {1: [{'id': 1, 'name': 'test'}], 2: [{'id': 2, 'name': 'test2'}, {'id': 2, 'name': 'test3'}]}
    """
    result = {}

    for item in data:
        if key not in item.keys():
            continue
        if item[key] not in result:
            result[item[key]] = []
        result[item[key]].append(item)

    return result


def map2map_list(data):
    """
    data的key,value 均为 int或str
    根据key将 map 重新生成新的 map list
    :param data: {1: 3, 2: 3, 4: 5}
    :return: {3: [1, 2], 5: [4]}
    """
    result = {}

    for k, v in data.items():
        if v not in result:
            result[v] = []
        result[v].append(k)

    return result


def list_map_set_by_key(data, key):
    """
    根据map中key将 list 去重 重新生成新的 list map
    :param data: [{}]
    :param key:
    :return:
    """
    new_data = []  # 用于存储去重后的list
    values = []  # 用于存储当前已有的值
    for item in data:
        if key not in item:
            continue

        if item[key] not in values:
            new_data.append(item)
            values.append(item[key])
    return new_data


def valid_date(str_date):
    """判断日期格式是否正确"""
    if not str_date:
        return False
    try:
        # time.strptime(str_date, "%Y-%m-%d")
        if "-" in str_date:
            time.strptime(str_date, "%Y-%m-%d")
        else:
            time.strptime(str_date, "%Y/%m/%d")
        return True
    except Exception as e:
        pass

    return False


def send_chunk(fb):
    """
    流式读取，用于文件下载
    :param fb: tempfile对象
    :return:
    """
    while True:
        # 每次读取20M
        chunk = fb.read(20 * 1024 * 1024)
        if not chunk:
            break
        yield chunk


def send_chunk_by_file(file_path, read_size=2 * 1024 * 1024):
    """
    流式读取，用于文件下载
    :param file_path: 文件
    :param read_size:
    :return:
    """
    with open(file_path, 'rb') as fb:
        while True:
            # 每次读取2M
            chunk = fb.read(read_size)
            if not chunk:
                break
            yield chunk


def seconds_to_time(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    day, h = divmod(h, 24)
    return "%d天%02d:%02d:%02d" % (day, h, m, s)


def timestamp2time(timestamp):
    time_struct = time.localtime(timestamp)
    return time.strftime('%Y-%m-%d %H:%M:%S', time_struct)


def unit_conversion(fsize):
    units = ['b', 'Kb', 'Mb', 'Gb', 'Tb']

    for idx, unit in enumerate(units):
        if fsize < 1024:
            break
        fsize /= 1024
        # fsize, decimal = divmod(fsize, 1024)
    if unit != units[0]:
        fsize = "%.2f" % fsize
    return f'{fsize}{unit}'


def timezone_converter_timestamp(str_time, fmt='%Y-%m-%dT%H:%M:%S', hours=8):
    # 零时区
    t = datetime.datetime.strptime(str_time, fmt)
    # 东八区
    t += datetime.timedelta(hours=hours)

    # return t.strftime('%Y-%m-%d %H:%M:%S')
    return int(time.mktime(t.timetuple()))


def rate_unit_conversion(fsize):
    units = ['bps', 'Kbps', 'Mbps', 'Gbps', 'Tbps']

    for idx, unit in enumerate(units):
        if fsize < 1024:
            break
        fsize /= 1024
        # fsize, decimal = divmod(fsize, 1024)
    if unit != units[0]:
        fsize = "%.2f" % fsize
    return f'{fsize}{unit}'
