# -*- coding：utf8 -*-
# @Project : read-file
# @Author : qxq
# @Time : 2021/12/17 10:41
import flask
from flask import jsonify, make_response


class StatusCode(object):
    OK = (200, u'操作成功')
    SERVERERR = (4001, u'服务异常')
    NODATA = (4002, u'无数据')
    DATAEXIST = (4003, u'数据已存在')
    DATAERR = (4004, u'数据错误')
    PARAMERR = (4005, u'参数错误')
    NETWORKERR = (4006, u'网络异常')
    ACCESSERR = (4007, u'权限错误')
    NOTLOGIN = (4008, u'没有登录')
    CONNERR = (4009, u'连接错误')
    NOTBIND = (4010, u'微信未绑定')
    NOTALLOWED = (4011, u'无资源')


class Response(object):
    @classmethod
    def success(cls, data=None, msg=None):
        """Response.success()
        success response when request processed

        :param data: data response to exact request
        :param msg:
        """
        res = {
            'code': StatusCode.OK[0],
            'msg': StatusCode.OK[1]
        }

        if msg is not None:
            res['msg'] = msg

        if data is not None:
            res['data'] = data

        return jsonify(res)

    @classmethod
    def error(cls, code=StatusCode.SERVERERR, msg=None, data=None):
        """
        error response when request processed

        :param code:
        :param msg:
        :param data: data response to exact request
        """
        if not isinstance(code, tuple) or len(code) != 2:
            raise Exception('server error')

        res = {
            'code': code[0],
            'msg': code[1]
        }

        if msg is not None:
            res['msg'] = msg

        if data is not None:
            res['data'] = data

        return jsonify(res)

    @classmethod
    def not_found(cls, msg=None):
        """
        error response when request processed

        :param msg:
        """
        res = {
            'code': StatusCode.NOTALLOWED[0],
            'msg': msg or StatusCode.NOTALLOWED[1]
        }

        return jsonify(res)

    @classmethod
    def not_allowed(cls, msg=None):
        """
        error response when request processed

        :param msg:
        """
        res = {
            'code': StatusCode.NOTALLOWED[0],
            'msg': msg or StatusCode.NOTALLOWED[1]
        }

        return jsonify(res)

    @classmethod
    def server_error(cls, msg=None):
        """
        error response when server internal error occured

        :param msg:
        """
        res = {
            'code': StatusCode.SERVERERR[0],
            'msg': msg or StatusCode.SERVERERR[1]
        }

        return jsonify(res)


class ResponseStream(object):
    @classmethod
    def success(cls, fb_stream, filename):
        """
        success response when request processed
        file obj stream
        :param fb_stream:
        :param filename:
        :return:
        """
        content_type = 'application/octet-stream;charset=UTF-8'
        # content_type = 'application/vnd.ms-excel'
        headers = {
            "Content-disposition": f"attachment; filename={filename}",
            "Access-Control-Expose-Headers": 'Content-Disposition',
        }

        response = flask.Response(fb_stream, content_type=content_type, headers=headers)

        # response = make_response(fb_stream)
        # response.headers['Content-type'] = 'application/octet-stream'
        # # response.headers['Content-type'] = 'application/vnd.ms-excel'  # 响应头告诉浏览器发送的文件类型为excel
        # response.headers['Content-Disposition'] = 'attachment; filename=data.xls'  # 浏览器打开/保存的对话框，data.xls-设定的文件名
        # response.headers['Access-Control-Expose-Headers'] = 'Content-Disposition'

        return response
