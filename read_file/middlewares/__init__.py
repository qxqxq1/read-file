# -*- coding：utf8 -*-
# @Project : read-file
# @Author : qxq
# @Time : 2021/12/17 10:41
from flask import request, g

from read_file.exceptions import APIException
from read_file.tools.resp_code import StatusCode, Response
from log import record_system_log
from utils.share import SharedMemory


class MiddlewareManger(object):
    """
    中间件管理类
    """

    def __init__(self, app=None):
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        @app.before_request
        def pre_process():
            return self.before_request()

        @app.after_request
        def post_process(response):
            return self.after_request(response)

    def before_request(self):
        """
        This call before the request to be processing
        """
        # current_app.logger.info(f'time: {datetime.datetime.now()}, path：{request.path}, endpoint：{request.endpoint}')
        return

    def after_request(self, response):
        """
        This call after the request to be processed
        """
        # TODO 记录请求日志
        # current_app.logger.info(f'time: {datetime.datetime.now()}, path：{request.path}, endpoint：{request.endpoint}')
        return response
