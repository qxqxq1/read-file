# -*- coding：utf8 -*-
# @Project : read-file
# @Author : qxq
# @Time : 2022/3/14 17:55
from utils.share import SharedMemory

from read_file import create_app

app = create_app()
SharedMemory.app = app
# flask
# flask-sqlalchemy = "==2.5.1"
# flask-cors = "==3.0.10"
# flask-wtf = "==0.15.1"
# eventlet = "==0.25.2"
# redis = "==3.5.3"
# pymysql = "==0.9.3"
# pyyaml = "==5.2"
# loguru = "==0.5.3"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=SharedMemory.Config.HTTP_PORT)
