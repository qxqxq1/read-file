# -*- coding：utf8 -*-
# @Project : read-file
# @Author : qxq
# @Time : 2021/12/17 10:41
import logging
import os
from yaml import dump, load

try:
    from yaml import Cloader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


class YAMLConfig(object):
    """解析系统配置文件"""

    @staticmethod
    def parse_app_config(path, filename):
        file_path = "{}/{}".format(path, filename)
        if not os.path.exists(file_path):
            YAMLConfig.write(
                DefaultConfig.get_app_default_config(), path, filename)

        with open(file_path, 'r') as fh:
            return load(fh, Loader=Loader)

    """
    写入默认数据
    """

    @staticmethod
    def write(data, path, filename):
        if not os.path.exists(path):
            os.makedirs(path)

        with open("{}/{}".format(path, filename), 'w') as fh:
            return fh.write(data)


class DefaultConfig(object):
    @staticmethod
    def get_app_default_config():
        return """# read-file配置文件
APP_ENV: dev
APP_NAME: read-file
DEBUG: False
# log setting config
LOG_BASE_PATH: /tmp/read-file
LOG_LEVEL: error
LOG_MANAGER_CONFIG:
    LOG_HANDLERS:
        - name: system_log
          file: 'system/read-file_system_{time:YYYY-MM-DD}.log'

          filter: system
          options:
            retention: '30 days'
            format: '{time:YYYY-MM-DD HH:mm:ss}|{message}'

        - name: operate_log
          file: 'operate/read-file_operate_{time:YYYY-MM-DD}.log'
          filter: operate
          options:
            retention: '30 days'
            format: '{message}'

HTTP_PORT: 9030
"""


class ConfigManager(object):
    def __new__(cls):
        if not hasattr(cls, '_instance'):
            cls._instance = super().__new__(cls)

        return cls._instance

    def __init__(self):

        dir_path = os.path.dirname(os.path.realpath(__file__))
        conf = YAMLConfig.parse_app_config(dir_path, 'read-file.yaml')
        conf['CONF_PATH'] = os.path.join(dir_path, 'read-file.yaml')

        conf['BASE_PATH'] = dir_path
        conf['SECRET_KEY'] = 'PS/TNL7+DEb3wu8aKL3Q93+6rtX1hYzoggoE0QQ296LJt5/hBWaoiRZZ8S9X98Fuq2oZkbLgZhuYNCSKoB/z4w=='

        conf['LOG_LEVEL'] = getattr(logging, conf['LOG_LEVEL'].upper())

        self.__dict__ = conf


Config = ConfigManager()
