# -*- coding：utf8 -*-
# @Project : read-file
# @Author : qxq
# @Time : 2021/12/17 10:41
import threading

from settings import Config


class SharedMemory(object):
    app = None

    db = None
    redis_store = None

    Config = None

    # 线程锁
    mutex = threading.Lock()


if not SharedMemory.Config:
    SharedMemory.Config = Config
