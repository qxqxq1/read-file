# -*- coding：utf8 -*-
# @Project : read-file
# @Author : qxq
# @Time : 2021/12/17 10:41
from uuid import uuid4


def get_code():
    """
    获取uuid 去除 -
    :return:
    """
    return str(uuid4()).replace('-', '')


if __name__ == '__main__':
    print(get_code(), len(get_code()))
