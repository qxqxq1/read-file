# -*- coding：utf8 -*-
# @Project : read-file
# @Author : qxq
# @Time : 2021/12/17 10:41
import socket
import struct

from flask import request


def get_ip_address(ifname):
    """根据网卡获取IP地址

    :param ifname: 网口名称
    """
    import fcntl
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])


def get_local_ip():
    """获取本机内网IP"""
    local_ip = ""
    try:
        socket_objs = [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]
        ip_from_ip_port = [(s.connect(("8.8.8.8", 53)), s.getsockname()[
            0], s.close()) for s in socket_objs][0][1]
        local_ip = ip_from_ip_port
    except Exception as e:
        pass

    return local_ip or "127.0.0.1"


def ip2long(ip):
    """Convert an IP string to long"""
    packed_ip = socket.inet_aton(ip)
    return struct.unpack("!L", packed_ip)[0]


def long2ip(num):
    """Convert an long to IP string"""
    return socket.inet_ntoa(struct.pack('!L', num))


def get_client_ip():
    try:
        ip = request.headers['X-Forwarded-For'].split(',')[0]
    except:
        try:
            ip = request.remote_addr
        except:
            ip = 'None'
    return ip
