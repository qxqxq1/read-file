# -*- coding：utf8 -*-
# @Project : read-file
# @Author : qxq
# @Time : 2021/12/17 10:41
import json
import time
import traceback

from loguru import logger

from utils.get_ip import ip2long, get_local_ip, get_client_ip
from utils.share import SharedMemory


class LoggerManager(object):
    """customer log manager class to deal all system logs
    """

    def __init__(self):
        self.base_path = SharedMemory.Config.LOG_BASE_PATH
        self.config = SharedMemory.Config.LOG_MANAGER_CONFIG
        self.init()

    def init(self):
        """initialize the logger information"""
        for handler in self.config['LOG_HANDLERS']:
            def filter_handler(record, filter_flag=handler['filter']):
                return filter_flag in record['extra']

            logger.add('{}/{}'.format(self.base_path, handler['file']), filter=filter_handler, **handler['options'])


class Log(object):
    @staticmethod
    def system(message):
        """system log handler
        usage:
        Log.system([{level}, {content}, {params}])
        :param level: 可选值 ERROR, INFO, WRAN, DEBUG
        :param content: 消息内容
        :param params: 参数值, 字典或json串
        :param ip: IP 本机ip
        :param second_timestemp: 微秒级时间戳
        """
        if not isinstance(message, list) or len(message) != 3:
            raise Exception('log format error, message should be list and not empty')
        if isinstance(message[2], (dict, list)):
            message[2] = json.dumps(message[2])

        message.append(str(ip2long(get_local_ip())))
        message.append(str(int(time.time() * 1000 * 1000)))
        logger.bind(system=True).info('|'.join(message))

    @staticmethod
    def operate(message):
        """operate log handler
        usage:
        Log.operate([{user_id}, {username}, {bussiness}, {action}, {content}, {status}])
        :param user_id: 操作者ID
        :param username: 操作者名称
        :param bussiness: 业务类型 ...
        :param action: 动作 ...
        :param content: 操作内容详情，json串
        :param status: 操作状态，1成功，2失败
        :param ip: IP 本机ip
        :param second_timestemp: 微秒级时间戳
        """
        if not isinstance(message, list) or len(message) <= 0:
            raise Exception('log format error, message should be list and not empty')

        log_msg = '|'.join(message)
        logger.bind(operate=True).info(log_msg)


def record_operate_log(msg):
    try:
        msg_list = list(msg.values())
        Log.operate(msg_list)
    except Exception as e:
        params = {
            'type': '记录操作日志错误',
            'username': msg.get('username'),
            'bussiness': msg.get('business'),
            'action': msg.get('action'),
            'content': msg.get('content'),
            'status': msg.get('status'),
        }
        record_system_log(e, params, level='ERROR')


def record_system_log(content, params, level='ERROR', request=None):
    try:
        if not isinstance(content, str):
            params['exc_info'] = traceback.format_exc()
            if not params.get('filename', None):
                params['filename'] = content.__traceback__.tb_frame.f_globals["__file__"]
            if not params.get('lineno', None):
                params['lineno'] = content.__traceback__.tb_lineno
        if request:
            params['url'] = request.url
            params['args'] = request.args.to_dict()
            params['form'] = request.form.to_dict()
            params['json'] = request.get_json() or {}
            params['view_args'] = request.view_args
            params['headers'] = {k: v for k, v in request.headers.items()}
            # params['headers'] = request.headers.to_wsgi_list()
            params['client_ip'] = get_client_ip()
        Log.system([level, str(content), params])
    except Exception as e:
        pass
